import React from 'react';
import { Text, View, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';

//public dependencies
import { styles } from '../public/styles';

export class Home extends React.Component {
    static navigationOptions = {
        title: 'Welcome',
    };
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text>Hello, Navigation!</Text>
                <Button
                    onPress={() => navigate('Chat')}
                    title="Chat"
                />
            </View>
        );
    }
}