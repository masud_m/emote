import React from 'react';
import { AppRegistry, StyleSheet, Text, View, FlatList, TextInput, KeyboardAvoidingView, TouchableOpacity, Button } from 'react-native';
import ReversedFlatList from 'react-native-reversed-flat-list';
import SocketIOClient from 'socket.io-client';
import { StackNavigator } from 'react-navigation';

//public dependencies
import { styles } from './public/styles';

//screens
import { Home } from './components/Home';

export class App extends React.Component {
    static navigationOptions = {
        title: 'Chat',
    };

    constructor(props) {
        super(props);

        // Creating the socket-client instance will automatically connect to the server.
        this.socket = SocketIOClient('http://192.168.0.105:3000', {
            transports: ['websocket'] // you need to explicitly tell it to use websockets
        });

        this.socket.on('message', this.onReceivedMessage.bind(this));


        this.state = {
            messages: [],
            typing: ""
        };

        console.ignoredYellowBox = [
            'Setting a timer'
        ];
    }



    componentWillMount() {

    }

    renderItem({ item }) {
        if (item.id == 0) {
            return (
                <View style={[styles.message, styles.sendMsg]}>
                    <Text style={[styles.messageText, styles.sendMsgText]}>{item.message}</Text>
                </View>
            )
        } else {
            return (
                <View style={styles.message}>
                    <Text style={styles.messageText}>{item.message}</Text>
                </View>
            )
        }
    }

    async sendMessage() {
        //alert(this.state.typing);
        await this.socket.emit('message', this.state.typing);

        // set the component state (clears text input)
        this.setState({
            messages: this.state.messages.concat({ message: this.state.typing, id: 0 }),
            typing: ''
        });
    }

    onReceivedMessage(message) {
        if (this.refs.messagesRef) {
            this.setState({
                messages: this.state.messages.concat([message])
            });
        }
    }

    render() {
        return (
            <View style={styles.container} ref="messagesRef">
                <ReversedFlatList data={this.state.messages} renderItem={this.renderItem} keyExtractor={(item) => item.id+(Math.floor(Date.now() * Math.random()))}/>
                <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={Expo.Constants.statusBarHeight + 56}>
                    <View style={styles.footer}>
                        <TextInput
                            value={this.state.typing}
                            onChangeText={text => this.setState({typing: text})}
                            style={styles.input}
                            underlineColorAndroid="transparent"
                            placeholder="Type something nice"
                        />  
                        <TouchableOpacity onPress={this.sendMessage.bind(this)}>
                            <Text style={styles.send}>Send</Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </View>
        );
    }
}

export default SimpleApp = StackNavigator({
    Home: { screen: Home },
    Chat: { screen: App }
}, {
    navigationOptions: { headerStyle: { marginTop: Expo.Constants.statusBarHeight } }
  })