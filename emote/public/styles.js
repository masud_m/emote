import React from 'react';
import { StyleSheet } from 'react-native';

const styleConfig = {
    padding: 5,
    colour: '#23f499',
    backgroundColour: '#FFF',
    fontSize: 18
};

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: styleConfig.backgroundColour,
    },
    message: {
        flex: 1,
        flexDirection: 'row',
        padding: styleConfig.padding
    },
    messageText: {
        backgroundColor: 'red',
        fontSize: styleConfig.fontSize,
        maxWidth: '80%',
        padding: styleConfig.padding,
        borderRadius: 5,
        overflow: 'hidden',
        flexWrap: 'wrap'
    },
    sendMsg: {
        alignSelf: 'flex-end'
    },
    sendMsgText: {
        backgroundColor: styleConfig.colour
    },
    sender: {
        fontWeight: 'bold',
        paddingRight: 10,
    },
    footer: {
        flexDirection: 'row',
        backgroundColor: '#eee',
    },
    input: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        fontSize: 18,
        flex: 1,
    },
    send: {
        alignSelf: 'center',
        color: styleConfig.colour,
        fontSize: 16,
        fontWeight: 'bold',
        padding: 20,
    }
});