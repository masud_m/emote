var express = require('express');
var http = require('http')
var socketio = require('socket.io');

var app = express();
var server = http.Server(app);
var socket = socketio(server, {pingTimeout: 30000});
server.listen(3000, () => console.log('listening on *:3000'));

// The event will be called when a client is connected.
socket.on('connection', (socket) => {
  console.log('A client just joined on', socket.id);
	
	socket.on('message', (message) => {
		console.log(message);
		socket.broadcast.emit('message', {message: message, id: socket.id});
	});
});



/*
const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8080 });

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
  });

  ws.send('something');
});*/